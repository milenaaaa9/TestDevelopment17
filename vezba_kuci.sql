--IZLISTATI SVE KOMEDIJE 
SELECT * FROM filmovi
WHERE filmovi.zanr = 'komedija';

--IZLISTATI SVE GLUMCE, POL NEBITAN, STARIJE OD...
SELECT * FROM glumci 
WHERE god_rodjenja < 1968;

--IZLISTATI SVE GLUMICE KOJE SU IZ SAD.
SELECT glumci.ime, glumci.prezime, drzava.naziv 
FROM glumci JOIN drzava 
ON glumci.drzavljanin = drzava.IDdrzava
WHERE drzava.naziv = 'SAD' AND glumci.pol = 'zenski';

--SVI FILMOVI KOJI SU SNIMLJENI U PRETHODNE 3 GODINE U SRBIJI
SELECT * FROM filmovi
JOIN drzava
WHERE drzava.naziv = 'Srbija' AND filmovi.godina > 1986;

--IZLISTATI PO KOLIKO JE FILMOVA SNIMLJENO U DRZAVAMA
SELECT COUNT('BROJ FILMOVA'), drzava.naziv 
FROM filmovi JOIN drzava
ON filmovi.drzava_porekla = drzava.IDdrzava
GROUP BY drzava.naziv;

--IZLISTATI FILMOVE NA KOJIMA JE ANGAZOVAN BAR JEDAN GLUMAC/GLUMICA IZ DRZAVE
U KOJOJ JE FILM I SNIMAN
SELECT * FROM filmovi
JOIN filmovi_glumci
ON filmovi.IDfilmovi = filmovi_glumci.IDfilmovi
JOIN glumci
ON glumci.IDglumci = filmovi_glumci.IDglumci
WHERE glumci.drzavljanin = filmovi.drzava_porekla;
